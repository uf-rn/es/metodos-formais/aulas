#ifndef _Ticket_f_h
#define _Ticket_f_h

#include <stdint.h>
#include <stdbool.h>
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Clause SETS */

/* Clause CONCRETE_VARIABLES */


/* Clause CONCRETE_CONSTANTS */
/* Basic constants */
/* Array and record constants */
extern void Ticket_f__INITIALISATION(void);

/* Clause OPERATIONS */

extern void Ticket_f__serve_next(int32_t *ss);
extern void Ticket_f__take_ticket(int32_t *tt);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _Ticket_f_h */
