#include "stdio.h"
#include "Ticket_f_i.c"

int main (int argc, char** argv)
{
    printf("STARTING...!\n");
    
    Ticket_f__INITIALISATION();

    int32_t answer;

    Ticket_f__take_ticket(&answer);
    printf("Take ticket %d\n", answer);

    Ticket_f__take_ticket(&answer);
    printf("Take ticket %d\n", answer);

    Ticket_f__take_ticket(&answer);
    printf("Take ticket %d\n", answer);

    Ticket_f__serve_next(&answer);
    printf("Serve next %d\n", answer);

    Ticket_f__serve_next(&answer);
    printf("Serve next %d\n", answer);

    Ticket_f__take_ticket(&answer);
    printf("Take ticket %d\n", answer);

    return (0);
}
