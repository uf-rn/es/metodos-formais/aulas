/* WARNING if type checker is not performed, translation could contain errors ! */

#include "Ticket_f.h"

/* Clause CONCRETE_CONSTANTS */
/* Basic constants */

/* Array and record constants */
/* Clause CONCRETE_VARIABLES */

static int32_t Ticket_f__serve;
static int32_t Ticket_f__next;
/* Clause INITIALISATION */
void Ticket_f__INITIALISATION(void)
{
    
    Ticket_f__serve = 0;
    Ticket_f__next = 0;
}

/* Clause OPERATIONS */

void Ticket_f__serve_next(int32_t *ss)
{
    (*ss) = Ticket_f__serve+1;
    Ticket_f__serve = Ticket_f__serve+1;
}

void Ticket_f__take_ticket(int32_t *tt)
{
    (*tt) = Ticket_f__next;
    Ticket_f__next = Ticket_f__next+1;
}

