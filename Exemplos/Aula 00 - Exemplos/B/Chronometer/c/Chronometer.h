#ifndef _Chronometer_h
#define _Chronometer_h

#include <stdint.h>
#include <stdbool.h>
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Clause SETS */

/* Clause CONCRETE_VARIABLES */


/* Clause CONCRETE_CONSTANTS */
/* Basic constants */
#define Chronometer__RANGE   059
/* Array and record constants */

extern void Chronometer__INITIALISATION(void);

/* Clause OPERATIONS */

extern void Chronometer__IncSec(void);
extern void Chronometer__IncMin(void);
extern void Chronometer__getMinutes(int32_t *answer);
extern void Chronometer__getSeconds(int32_t *answer);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _Chronometer_h */
