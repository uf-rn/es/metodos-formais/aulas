#include "stdio.h"
#include "Chronometer_i.c"

int main (int argc, char** argv)
{
    printf("STARTING...!\n");
    
    Chronometer__INITIALISATION();

    int32_t answer;

    printf("---------------------\n");
    Chronometer__getMinutes(&answer);
    printf("Minutes: %d\n", answer);
    Chronometer__getSeconds(&answer);
    printf("Seconds %d\n", answer);
    printf("---------------------\n");

    printf("Incrementing 2 seconds...\n");
    Chronometer__IncSec();
    Chronometer__IncSec();

    printf("---------------------\n");
    Chronometer__getMinutes(&answer);
    printf("Minutes: %d\n", answer);
    Chronometer__getSeconds(&answer);
    printf("Seconds %d\n", answer);
    printf("---------------------\n");

    printf("Incrementing 61 seconds...\n");
    int a;
    for(a = 0; a < 61; a = a + 1 ){
      Chronometer__IncSec();
    }    

    printf("---------------------\n");
    Chronometer__getMinutes(&answer);
    printf("Minutes: %d\n", answer);
    Chronometer__getSeconds(&answer);
    printf("Seconds %d\n", answer);
    printf("---------------------\n");

    return (0);
}
