/* WARNING if type checker is not performed, translation could contain errors ! */

#include "Chronometer.h"

/* Clause CONCRETE_CONSTANTS */
/* Basic constants */

/* Array and record constants */
/* Clause CONCRETE_VARIABLES */

static int32_t Chronometer__seconds;
static int32_t Chronometer__minutes;
/* Clause INITIALISATION */
void Chronometer__INITIALISATION(void)
{
    
    Chronometer__seconds = 0;
    Chronometer__minutes = 0;
}

/* Clause OPERATIONS */

void Chronometer__IncSec(void)
{
    Chronometer__seconds = (Chronometer__seconds+1) % 60;
    if(Chronometer__seconds == 0)
    {
        Chronometer__minutes = (Chronometer__minutes+1) % 60;
    }
    else
    {
        ;
    }
}

void Chronometer__IncMin(void)
{
    Chronometer__minutes = (Chronometer__minutes+1) % 60;
}

void Chronometer__getMinutes(int32_t *answer)
{
    (*answer) = Chronometer__minutes;
}

void Chronometer__getSeconds(int32_t *answer)
{
    (*answer) = Chronometer__seconds;
}

