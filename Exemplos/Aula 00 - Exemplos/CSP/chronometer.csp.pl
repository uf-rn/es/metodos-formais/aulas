:- dynamic parserVersionNum/1, parserVersionStr/1, parseResult/5.
:- dynamic module/4.
'parserVersionStr'('0.6.2.1').
'parseResult'('ok','',0,0,0).
:- dynamic channel/2, bindval/3, agent/3.
:- dynamic agent_curry/3, symbol/4.
:- dynamic dataTypeDef/2, subTypeDef/2, nameType/2.
:- dynamic cspTransparent/1.
:- dynamic cspPrint/1.
:- dynamic pragma/1.
:- dynamic comment/2.
:- dynamic assertBool/1, assertRef/5, assertTauPrio/6.
:- dynamic assertModelCheckExt/4, assertModelCheck/3.
:- dynamic assertLtl/4, assertCtl/4.
'parserVersionNum'([0,11,1,1]).
'parserVersionStr'('CSPM-Frontent-0.11.1.1').
'channel'('tick','type'('dotUnitType')).
'channel'('time','type'('dotUnitType')).
'channel'('out','type'('dotTupleType'(['setExp'('rangeClosed'('int'(0),'int'(59))),'setExp'('rangeClosed'('int'(0),'int'(59)))]))).
'agent'('chrono'(_min,_sec),'[]'('prefix'('src_span'(5,2,5,6,67,4),[],'tick','ifte'('=='(_sec,'int'(59)),'agent_call'('src_span'(6,15,6,21,103,6),'chrono',['%'('+'(_min,'int'(1)),'int'(60)),'int'(0)]),'agent_call'('src_span'(7,13,7,19,138,6),'chrono',[_min,'+'(_sec,'int'(1))]),'no_loc_info_available','no_loc_info_available','src_span'(6,38,7,12,125,52)),'src_span'(5,7,5,9,71,89)),'prefix'('src_span'(9,2,9,6,162,4),[],'time','prefix'('src_span'(9,10,9,13,170,3),['out'(_min),'out'(_sec)],'out','agent_call'('src_span'(9,25,9,31,185,6),'chrono',[_min,_sec]),'src_span'(9,22,9,24,181,23)),'src_span'(9,7,9,9,166,38)),'src_span_operator'('no_loc_info_available','src_span'(8,2,8,4,158,2))),'no_loc_info_available').
'bindval'('Chronometer','agent_call'('src_span'(10,15,10,21,215,6),'chrono',['int'(0),'int'(0)]),'src_span'(10,1,10,26,201,25)).
'bindval'('User','prefix'('src_span'(12,8,12,12,235,4),[],'time','prefix'('src_span'(12,16,12,19,243,3),['in'(_x),'in'(_y)],'out','val_of'('User','src_span'(12,27,12,31,254,4)),'src_span'(12,24,12,26,250,10)),'src_span'(12,13,12,15,239,23)),'src_span'(12,1,12,31,228,30)).
'bindval'('System','sharing'('Events','val_of'('Chronometer','src_span'(14,10,14,21,269,11)),'val_of'('User','src_span'(14,35,14,39,294,4)),'src_span'(14,22,14,34,281,12)),'src_span'(14,1,14,39,260,38)).
'assertModelCheck'('False','val_of'('System','src_span'(16,8,16,14,307,6)),'DeadlockFree').
'symbol'('tick','tick','src_span'(1,9,1,13,8,4),'Channel').
'symbol'('time','time','src_span'(1,15,1,19,14,4),'Channel').
'symbol'('out','out','src_span'(2,9,2,12,27,3),'Channel').
'symbol'('chrono','chrono','src_span'(4,1,4,7,48,6),'Funktion or Process').
'symbol'('min','min','src_span'(4,8,4,11,55,3),'Ident (Prolog Variable)').
'symbol'('sec','sec','src_span'(4,12,4,15,59,3),'Ident (Prolog Variable)').
'symbol'('Chronometer','Chronometer','src_span'(10,1,10,12,201,11),'Ident (Groundrep.)').
'symbol'('User','User','src_span'(12,1,12,5,228,4),'Ident (Groundrep.)').
'symbol'('x','x','src_span'(12,20,12,21,247,1),'Ident (Prolog Variable)').
'symbol'('y','y','src_span'(12,22,12,23,249,1),'Ident (Prolog Variable)').
'symbol'('System','System','src_span'(14,1,14,7,260,6),'Ident (Groundrep.)').