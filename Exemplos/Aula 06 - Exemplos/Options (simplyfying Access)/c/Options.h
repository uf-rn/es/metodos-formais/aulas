#ifndef _Options_h
#define _Options_h

#include <stdint.h>
#include <stdbool.h>
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define Options__USER__max 1
#define Options__PRINTER__max 10
#define Options__OPTION__max 10
/* Clause SETS */
typedef int Options__USER;
typedef int Options__PRINTER;
typedef int Options__OPTION;
typedef enum
{
    Options__ok,
    Options__no
    
} Options__PERMISSION;
#define Options__PERMISSION__max 2

/* Clause CONCRETE_VARIABLES */


/* Clause CONCRETE_CONSTANTS */
/* Basic constants */
/* Array and record constants */
extern void Options__INITIALISATION(void);

/* Clause OPERATIONS */

extern void Options__add(Options__PRINTER pp, Options__OPTION oo);
extern void Options__remove(Options__PRINTER pp, Options__OPTION oo);
extern void Options__optionquery(Options__PRINTER pp, Options__OPTION oo, Options__PERMISSION *ans);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _Options_h */
