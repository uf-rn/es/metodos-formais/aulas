#include "stdio.h"
#include "Options_i.c"

int main (int argc, char** argv)
{
    printf("STARTING...!\n");
    
    Options__INITIALISATION();

    Options__add(10, 9);
    Options__add(10, 10);

    unsigned int i = 0, j= 0;
    for(i = 0; i <= 10;i++)
    {
        for(j = 0; j <= 10;j++)
        {
            printf("Printer %d - Option %d = %d\n", i, j, Options__options_i[i][j]);
        }
    }
    return (0);


}
