/* WARNING if type checker is not performed, translation could contain errors ! */

#include "Options.h"

/* Clause CONCRETE_CONSTANTS */
/* Basic constants */

/* Array and record constants */
/* Clause CONCRETE_VARIABLES */

static bool Options__options_i[11][11];
/* Clause INITIALISATION */
void Options__INITIALISATION(void)
{
    
    unsigned int i = 0, j= 0;
    for(i = 0; i <= 10;i++)
    {
        for(j = 0; j <= 10;j++)
        {
            Options__options_i[i][j] = false;
        }
    }
}

/* Clause OPERATIONS */

void Options__add(Options__PRINTER pp, Options__OPTION oo)
{
    Options__options_i[pp][oo] = true;
}

void Options__remove(Options__PRINTER pp, Options__OPTION oo)
{
    Options__options_i[pp][oo] = false;
}

void Options__optionquery(Options__PRINTER pp, Options__OPTION oo, Options__PERMISSION *ans)
{
    {
        bool bb;
        
        bb = Options__options_i[pp][oo];
        if(bb == true)
        {
            (*ans) = Options__ok;
        }
        else
        {
            (*ans) = Options__no;
        }
    }
}

